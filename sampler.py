#!/usr/bin/env python

# coding: utf-8

# Solution to the Citrine Informatics Technical Challenge (Alireza Khorshidi):

# Let us start by first importing a number of handy python libraries:

# In[1]:


import numpy as np
import math


# The following class has been kindly provided by the examiners which makes my
# life easier! :) As the docstring of this class says, it takes a file name as
# input and constructs a Constraint object based on that:

# In[2]:


class Constraint():
    """Constraints loaded from a file."""

    def __init__(self, fname):
        """
        Construct a Constraint object from a constraints file

        :param fname: Name of the file to read the Constraint from (string)
        """
        with open(fname, "r") as f:
            lines = f.readlines()
        # Parse the dimension from the first line
        self.n_dim = int(lines[0])
        # Parse the example from the second line
        self.example = [float(x) for x in lines[1].split(" ")[0:self.n_dim]]

        # Run through the rest of the lines and compile the constraints
        self.exprs = []
        for i in range(2, len(lines)):
            # support comments in the first line
            if lines[i][0] == "#":
                continue
            self.exprs.append(compile(lines[i], "<string>", "eval"))
        # Additional constraints to make sure the sample point is within the
        # unit hypecube are added to self.exprs.
        for i in range(self.n_dim):
            self.exprs.append('x[%d] >= 0' % i)
            self.exprs.append('1 - x[%d] >= 0' % i)
        return

    def get_example(self):
        """Get the example feasible vector"""
        return self.example

    def get_ndim(self):
        """Get the dimension of the space on which the constraints are defined.
        """
        return self.n_dim

    def apply(self, x):
        """
        Apply the constraints to a vector, returning True only if all are
        satisfied.

        :param x: list or array on which to evaluate the constraints
        """
        for expr in self.exprs:
            if not eval(expr):
                return False
        return True

# From here on the real solution begins. In the simplest possible algorithm we
# could draw random points in the N-dimensional space and then check whether or
# not it belongs to the feasible space. If the random point belongs to the
# feasible space, then we accept it and draw the next point. If not, we reject
# it and redraw another random point. We keep doing that until we have a set
# of n random sampling points.

# This simple algorithm however, might take a lot of time, depending on the
# size of the feasible space. If the size of the feasible space is small, then
# random draws are likely to be rejected. In the other asymptotic case, if the
# size of the feasible space is infinite, then all draws are accepted, and the
# algorithm will be very fast.

# If we want to decrease the number of rejections we should draw the random
# points from a more limited space instead of the infinite space. The limited
# space should encompass the feasible space. The most straightforward limited
# space is probably an N-dimensional rectangular (hyper)box which encompasses
# the feasible space.

# The next challenge is finding the hyperbox that encompasses the feasible
# space. In order to achieve that we can use python packages for linear and
# nonlinear programming. (Linear programming for the case where the constraints
# are linear e.g. inputs "alloy.txt" and "mixture.txt", and nonlinear
# programming for the case where the constraints are nonlinear e.g. inputs
# "example.txt" and "formulation.txt"). Scipy's minimize module is a
# straightforwad and easy-to-use package for this purpose. So let us import
# the minimize module and write a wrapper around it:

# In[5]:


from scipy.optimize import minimize


# In[6]:


def scipy_minimizer(f, x0, args, constraints):
    """
    Wrapper function around scipy minimize module. Takes an objective function
    f, a starting point x0 (within the feasible space for our purpose),
    arguments args of the objective function, and a set of constraints.
    constraints should be fed in special format of a dictionary with 'type'
    and 'fun' keys. We will get to that shortly.
    The wrapper returns the minimum value of the objective function.
    """
    solution = minimize(fun=f,
                        x0=x0,
                        method='SLSQP',
                        args=args,
                        # bounds=bnds,
                        constraints=constraints,
                        )
    return solution.fun


# The key idea is however the function that we select to minimize in order to
# get the lower and upper bounds of the encompassing (hyper)box. In order to
# find the boundaries of the encompassing (hyper)box, we minimize the functions
# f=x[i] and f=-x[i] for i=0, 1, ..., N-1, subject to constraints.

# ***Note***: This function requires the scitools package installed. See
# instructions for how to install scitools at
#     https://github.com/hplgit/scitools.

# In[7]:


def generate_constraints_in_scipyminimize_form(filename):
    """This function takes a filename and returns its constraints in the form
    acceptable by the scipy.optimize.minimize module."""
    from scitools.StringFunction import StringFunction
    with open(filename, "r") as f:
        lines = f.readlines()
        constraints = []
        for i in range(2, len(lines)):
            if lines[i][0] == "#":
                continue
            constraints.append({})
            constraints[-1]['type'] = 'ineq'
            constraints[-1]['fun'] = \
                StringFunction(expression=lines[i].split(">=")[0],
                               independent_variable='x',
                               )
    return constraints


# In[8]:


def find_encompassing_hyperbox(filename, cons):
    """This function finds the (hyper)box that encompasses the feasible space
    and returns its boundaries. 
    Its input arguments are the filename and a constraint object in the form
    of Constraint() class."""
    def minimum_component(x, i):
        return x[i]

    def maximum_component(x, i):
        return -x[i]
    boundaries = []
    # convert the cons object to the form acceptable by
    # scipy.optimize.minimize module.
    scipycons = generate_constraints_in_scipyminimize_form(filename)
    x0 = cons.example
    for i in range(cons.get_ndim()):
        boundaries.append([])
        # finding the lower bound.
        lower_bound = scipy_minimizer(f=minimum_component,
                                      args=(i,),
                                      x0=x0,
                                      constraints=scipycons,
                                      )
        # The random point should be within the hypercube.
        if (math.isnan(lower_bound) is True) or (lower_bound < 0.):
            lower_bound = 0.
        boundaries[i].append(lower_bound)
        # finding the upper bound.
        upper_bound = scipy_minimizer(f=maximum_component,
                                      args=(i,),
                                      x0=x0,
                                      constraints=scipycons,
                                      )
        # The random point should be within the hypercube.
        if (math.isnan(upper_bound) is True) or (upper_bound < -1.):
            upper_bound = -1.
        boundaries[i].append(-upper_bound)
    return boundaries


# We can look at the boundaries of the box in case we see any suspicious
# behavior:

# Now we put all parts together. We write a function (take_samples) that
# assembles different pieces together. This function first finds the (hyper)box
# that encompasses the feasible space. It then draws random points from the
# encompassing (hyper)box until n of them within the feasible space is found.

# The important point is that I use a uniform sampler for taking the random
# samples, which means that samples come from a uniform distribution.
# That seems to be the best choice since we want the points spread out inside
# the feasible space. (We don't want them being sampled from a particular
# region within the feasible space (either purposely or unintentionally).)
# Therefore sampling from a uniform distribution seems to be the best choice.
# Here we first import a random sampler from a uniform distribution.
# We then write a simple function that draws random samples from a uniform
# distribution given the boundaries of the (hyper)box. Finally, we see the
# assembling function 'take_samples'.

# In[10]:


from random import uniform


# In[11]:


def draw_a_random_point_within_hyperbox(boundaries):
    """This function draws a random sample from a uniform distribution given
    the lower and upper bounds by 'boundaries'."""
    x = []
    for boundary in boundaries:
        random_value = uniform(boundary[0], boundary[1])
        x.append(random_value)
    return x


# In[12]:


def take_samples(filename, n_results):
    """
    This function assembles different pieces that we had above. The function
    takes a filename and number of results
    'n_results', and returns feasible samples. The function starts by finding
    the encompassing hyperbox. Then it tries random points within the
    encompassing box to find n points within the feasible space.
    """
    cons = Constraint(filename)
    # finding the boundaries of the encompassing (hyper)box
    boundaries = find_encompassing_hyperbox(filename, cons)
    feasible_samples = []
    # taking n samples
    for _ in range(n_results):
        # for each sample, we draw a random point in the hyperbox. If it falls
        # inside the feasible space good! If not, try another random point.
        within_feasible_space = False
        while within_feasible_space is False:
            x = draw_a_random_point_within_hyperbox(boundaries)
            within_feasible_space = cons.apply(x)
        feasible_samples.append(x)
    return feasible_samples


# Let us call this function for 1000 points and see the results:

# We can know double check the sample points and make sure they are all within
# the feasible space. Ideally the following call should print nothing!

# The following is just parsing the results into a text file:

# In[ ]:


def parse_to_text(filename, samples):
    with open(filename, "w") as f:
        for sample in samples:
            for component in sample:
                f.write(str(component))
                f.write(' ')
            f.write('\n')


# And finally, this is to make the python script executable:

# In[ ]:


def main():
    import sys
    try:
        from scitools.StringFunction import StringFunction
    except ImportError:
        msg = """This module depends on scitools python library, which could
        not be imported. For instructions on how to install scitools, please
        visit https://github.com/hplgit/scitools"""
        print(msg)
        
    input_filename = sys.argv[1]
    output_filename = sys.argv[2]
    n_results = int(sys.argv[3])
    samples = take_samples(input_filename, n_results)
    parse_to_text(output_filename, samples)

#    # This is just a check to make sure all samples satisfy the constraints.
#    cons = Constraint(input_filename)
#    for sample in samples:
#        within_feasible = cons.apply(sample)
#        if within_feasible is False:
#            print("within feasible space?", within_feasible)



if __name__ == "__main__":
    main()
