Thanks for your time and interest! This is the solution to the Citrine Informatics Technical Challenge for the Scientific Software Engineer position prepared by Alireza Khorshidi.

Please first do the following in your terminal to clone this repository:

$ git clone git@bitbucket.org:akhorshi/citrine_challenge.git

The repository contains a pdf file discussing my solution named "Citrine_Technical_Challenge_Alireza_Khorshidi.pdf", an ipython notebook named "Citrine_Technical_Challenge_Alireza_Khorshidi.ipynb" which is basically an interactive mode of the pdf file, and finally a python script named "sampler.py" which has essentially the same discussion details as that of the pdf and ipynb files. The example text files are included in the repository as well.

PDF: If you don't want to have the pain of installation of the required library and running the script, but just want to take a look at my solution, this is the file you should look at.

IPYNB: If you want to see my discussions but at the same time run the script in a beautiful environment, this is the file to take a look at.

PYTHON SCRIPT: If you want to execute the script from your terminal, you should go with this file. Detailed discussions and docstrings exist in the python script as well as commented lines.


INSTRUCTIONS FOR TWO-STEP INSTALLATION:

STEP 1: This module relies on the external python library scitools. In order to install scitools, please follow the detailed installation instructions at https://github.com/hplgit/scitools. In short, if you are running the script on Linux-based systems Debian/Ubuntu, do in your terminal:

$ sudo apt-get install python-scitools

If you have Anocanda installed on your system, you can do the following in your terminal:

$ conda install --channel johannr scitools

And of course you can download the package from the github repository https://github.com/hplgit/scitools directly. Then go to the scitools directory and run

$ python setup.py install [,--prefix=$PREFIX]


STEP 2: If you want to execute the python script directly in your terminal, you should add the directory of this repository to your "PATH" variables, with something like:

$ export PATH=/path/to/this/repository/citrine_challenge:$PATH



HOW TO EXECUTE THE MODULE:

As an example you can simply do

$ ./sampler.py Examples/alloy.txt Examples/alloy_output.txt 1000

where the input file name is "Examples/alloy.txt", the output file name is "Examples/alloy_output.txt", and the number of samples is 1000.


 


